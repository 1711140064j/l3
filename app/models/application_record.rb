class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  validates :message, presence: true,
                      length: { minimum: 1, maximum: 140}
end
